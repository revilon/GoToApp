// Тут добавляем маленькую надпись поверх иконки расширения о текущем сервере
window.onload = function(){
    chrome.tabs.onUpdated.addListener(function(id, info, tab) { // Вкладка обновилась/открылась новая
        chrome.tabs.query({ // Извлекаем текущий url из вкладки
          "active": true,
          "currentWindow": true,
          "status": "complete",
          "windowType": "normal"
        }, function (tabs) {
            for (tab in tabs) {
                if (tabs[tab].url.split("/")[2] == 'online-samsung.ru') {
                    chrome.cookies.get({ url: 'https://online-samsung.ru/', name: 'SRVNAME' }, function (cookie) { // Смотрим что в куках 
                        if (cookie) {
                            chrome.browserAction.setBadgeText({text:cookie.value}); //Устанавливает текст поверх иконки
                            chrome.browserAction.setBadgeBackgroundColor({color:[0,0,0]});
                        }
                    });
                } else {
                    chrome.browserAction.setBadgeText({text:'und'}); //Устанавливает текст поверх иконки
                    chrome.browserAction.setBadgeBackgroundColor({color:[0,0,0]}); //Устанавливает фон текста поверх иконки
                }
            }
        });
/*        chrome.cookies.get({ url: 'https://online-samsung.ru/', name: 'SRVNAME' }, function (cookie) {
            if (cookie) {
                chrome.browserAction.setBadgeText({text:cookie.value}); //Устанавливает текст поверх иконки
                chrome.browserAction.setBadgeBackgroundColor({color:[0,0,0]});
            } else {
                chrome.browserAction.setBadgeText({text:'und'}); //Устанавливает текст поверх иконки
                chrome.browserAction.setBadgeBackgroundColor({color:[0,0,0]}); //Устанавливает фон текста поверх иконки
            }
        });*/
    });
}