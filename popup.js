document.addEventListener("DOMContentLoaded", function () {

    // Добавим возможность перехода на не зарегестрированный сервер по нажатию клавиши enter
    document.getElementById("unregisteredServ").addEventListener("keyup", function(event) {
        event.preventDefault();
        if (event.keyCode == 13) {
            document.getElementById("unregisteredServGo").click();
        }
    });

    // Проверяем url текущей вкладки
    chrome.tabs.query({ //This method output active URL 
        "active": true,
        "currentWindow": true,
        "status": "complete",
        "windowType": "normal"
    }, function (tabs) {
        for (tab in tabs) {
            if (tabs[tab].url.split("/")[2] == 'online-samsung.ru') {
                document.querySelector('div.loadPage').classList.add('hide');
                document.querySelector('div.go_to_serv').classList.remove('hide');
            } else {
                document.querySelector('div.loadPage').classList.add('hide');
                document.querySelector('div.pageNotValid').classList.remove('hide');
            }
        }
    });

    // Тут все кнопки получат свои функции для эвента клик
    var buttons = document.querySelectorAll('.jumpToServBtn');
    for (var i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener('click', function() {
            chrome.cookies.set({
                "name":"SRVNAME",
                "url":"https://online-samsung.ru/",
                "value": this.value
            });
            this.innerHTML = "Reloading Page...";
            chrome.tabs.getSelected(null, function(tab) {
                var code = 'window.location.reload();';
                chrome.tabs.executeScript(tab.id, {code: code});
                setTimeout(function() {
                    window.close();
                }, 1500)
            });
        });
    }

    // Тут добавим возможность переходить на не зарегестрированные сервера
    var unregisteredServBtn = document.querySelector('#unregisteredServGo'),
        unregisteredServInput = document.querySelector('.unregisteredServ');
    unregisteredServBtn.addEventListener('click', function() {
        if (unregisteredServInput.value.length > 0) {
            unregisteredServInput.classList.remove('errorInput');
            chrome.cookies.set({
                "name":"SRVNAME",
                "url":"https://online-samsung.ru/",
                "value": unregisteredServInput.value
            });
            this.innerHTML = "Reloading Page...";
            unregisteredServInput.disabled = true;
            chrome.tabs.getSelected(null, function(tab) {
                var code = 'window.location.reload();';
                chrome.tabs.executeScript(tab.id, {code: code});
                setTimeout(function() {
                    window.close();
                }, 1500)
            });
        } else {
            unregisteredServInput.classList.add('errorInput');
        }
    });

    // Получаем имя текущего сервера
    chrome.cookies.get({ url: 'https://online-samsung.ru/', name: 'SRVNAME' }, function (cookie) {
        if (cookie) {
            document.querySelector('div.nowServer').innerHTML = 'Now server: ' + cookie.value;
        } else {
          console.log('Can\'t get cookie! Check the name!');
        }
    });
});